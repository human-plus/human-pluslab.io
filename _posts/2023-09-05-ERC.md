---
title: OMG! ERC! 
author:   
layout: post
group: news
---
It's real! It's happening! Ruud received an ERC Starting Grant for the F-AI-MILY project! A super exciting and big step for Human+. From 2024 to 2029, Human+ will investigate the impact of AI on the social dynamics of families! [Read more about this here](https://www.uu.nl/en/news/erc-grant-to-research-communities-of-arms-and-artificial-intelligence-within-families)

There will be PhD, postdoc, and RA positions available! More information soon!

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">Absolutely trilled that I have been awarded an<a href="https://twitter.com/ERC_Research?ref_src=twsrc%5Etfw">@ERC_Research</a> Starting Grant! This is so massive I can barely comprehend. This one is for all my mentors, colleagues, friends, partner, family, and for little me who (almost) failed high school. 🥳🥳🥳F-AI-MILY is real and happening! <a href="https://t.co/J8gu4UeOms">https://t.co/J8gu4UeOms</a></p>&mdash; Ruud Hortensius @ruudhortensius@fediscience.org (@RuudHortensius) <a href="https://twitter.com/RuudHortensius/status/1699048190006190305?ref_src=twsrc%5Etfw">September 5, 2023</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>