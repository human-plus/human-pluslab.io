---
citation: "**Fabiola Diana**, Oscar E. Juárez-Mora, Wouter Boekel, **Ruud Hortensius**, Mariska E. Kret (2023). How video calls affect mimicry and trust during interactions. <i>Philosophical Transactions of the Royal Society B</i> "
date: '2023-03-06'
pdf: https://royalsocietypublishing.org/doi/10.1098/rstb.2021.0484
links:
- name: preprint
  url: https://psyarxiv.com/vrkdf/
- name: data, materials & code
  url: https://osf.io/kpb2u/
---