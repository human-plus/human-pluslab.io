---
citation: "**Fabiola Diana**, Misako Kawahara, Isabella Saccardi, **Ruud Hortensius**, Akihiro Tanaka, Mariska Kret (2022).  Implicit and explicit attitudes towards artificial agents: a cross-cultural comparison.  <i>International Journal of Social Robotics</i>"
date: '2022-09-28'
pdf: 'https://link.springer.com/article/10.1007/s12369-022-00917-7'
links:
- name: preprint
  url: https://psyarxiv.com/zu3pe/
- name: data, materials & code
  url: https://osf.io/uat6r/
---
