---
citation: "**Ruud Hortensius\\***, Michaela Kent\\*, Kohinoor Darda, Laura Jastrzab, Kami Koldewyn, Richard Ramsey, Emily S. Cross (2021). Exploring the relationship between anthropomorphism and Theory-of-Mind in brain and behaviour. <i>Human Brain Mapping</i> <br> *<i>joined first authorship<i>"
date: '2021-05-24'
pdf: 'https://onlinelibrary.wiley.com/doi/10.1002/hbm.25542'
links:
- name: preprint
  url: https://psyarxiv.com/3uj4g/
- name: preregistration
  url: https://osf.io/tuq4a/
- name: data
  url: https://osf.io/rg5kn/
- name: NeuroVault
  url: https://neurovault.org/collections/6615/
- name: code
  url: https://gitlab.com/hortensius/anthrom    
---
