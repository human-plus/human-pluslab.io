---
title: Human+ Research 
layout: default
group: research
---

# Social cognition in the age of robots and algorithms
Our research aims to understand human social cognition during real interactions with humans, robots and AI. While films, literature, and art have provided us with a rich depiction of the potential of robots and ai, our understanding of actual interactions with these new technologies remains limited. 
We not only explore how and when people form relationships with these new artificial intelligent technologies but also how these new interactions shape distinct aspects of social cognition (for example, emotion, reward, empathy, trust, social learning).

To map these changes in behaviour and brain, we use: 
- neuroimaging techniques (fMRI and fNIRS)  
- behavioural testing (play a game or solve a puzzle with an artificial intelligent agent)  
- together with tailormade socialising interventions (robots at home and in the public)   

<p align="center">
  <img width="75%" src="/static/img/nyas_overview.jpeg" />
</p>

<center>
<i>Functional convergence of cognitive factors driving socialness attribution to artificial agents within the Theory‐of‐Mind network. <br> From <a href="http://ruudhortensius.nl/pubs/HortensiusCross2018NYAS">Hortensius & Cross 2018</a></i>
</center>
<br>
#### Current focuses
- mapping the temporal and functional changes in the neural representation of social cognition during human-robot interaction 
- understanding relationship formation during child-robot interaction
- perceptual and behavioural consequences of interactions with embodied AI
- explore the influence of daily exposure to AI on social cognition
- the impact of ai on family dynamics 
<br>
<br>

<p align="center">
  <img width="60%" src="/static/img/tinsfnirs.jpg" />
</p>

<center>
<i>A research approach to get human-robot interactions out of the lab into the wild using functional Near-Infrared Spectroscopy. <br>  From <a href="https://www.cell.com/action/showPdf?pii=S0166-2236%2820%2930073-4">Henschel, Hortensius & Cross 2020 </a></i>
</center>
<br>
#### Open Science Statement
We are extremely lucky to explore new and exciting scientific questions on the dynamics of human social cognition. 
We can dive into the unknowns, provide new takes, re-establish foundations, and uncover new terrain. 
When doing so, we need to work together in an open and transparent manner. We adopt an open science approach and embrace preregistration, 
share our data, code, and materials, use open access publishing and preprints, and use contributor roles taxonomy to give credit during projects. 