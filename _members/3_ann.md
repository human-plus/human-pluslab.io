---
name: Ann Hogenhuis
startdate: [2012-01-01]
enddate: []
image: /static/img/members/ann.jpeg
# altimage: /static/img/members/[file].jpg
position: PhD-candidate
#current:
#email: 
#scholar: 
#website: 
#twitter: 
#github: 
#gitlab: 
#orcid: 
description: "Ann obtained her bachelor’s degree at Utrecht University in Liberal Arts and Sciences, where she developed a special interest in interdisciplinary sciences with a major in social cognitive neuroscience. After graduating, she continued with the research master in Cognitive Neuroscience at the Donders Institute (Radboud University), focusing on the neural mechanisms of human-robot interaction in both her BSc and MSc theses. During her MSc internship she visited the University of Glasgow for half a year to conduct functional connectivity analyses. Her ambition for understanding social interactions within our brain continued to grow, and her current Ph.D. project investigates the developmental aspects of group dynamics and the role AI may play in shaping these interactions. In her free time, Ann enjoys photography, sewing, wandering through playlists for new music, but mostly spending time with coffee and friends.
"
---
