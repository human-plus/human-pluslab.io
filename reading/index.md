---
title: Journal club
layout: default
group: reading
---

# Journal club


We have a weekly journal club, in which we discuss papers relevant for ongoing projects in the lab 
(e.g., inter-brain synchrony, family dynamics, human-AI interaction, social neuroscience, fNIRS, neuroimaging, data analysis). 
One person picks the article, introduces the topic, and leads the discussion. Below you will find an overview of articles we discussed. If you want to join, let us know!

**Season 1**:

<table>
<colgroup>
<col width="10%" />
<col width="90%" />
</colgroup>
<thead>
<tr class="header">
<th>Episode</th>
<th>Article</th>
</tr>
</thead>
<tbody>
<tr>
<td markdown="span">S01E01</td>
<td markdown="span">Hamilton, A. F. D. C. (2021). Hyperscanning: Beyond the Hype. *Neuron*, 109(3), 404–407. [link](https://doi.org/10.1016/j.neuron.2020.11.008)</td>
</tr>
<tr>
<td markdown="span">S01E02</td>
<td markdown="span">Ibanez, A. (2022). The mind’s golden cage and cognition in the wild. *Trends in Cognitive Sciences*, 26(12), 1031-1034. [link](https://www.sciencedirect.com/science/article/pii/S1364661322001656)</td>
</tr>
<tr>
<td markdown="span">S01E03</td>
<td markdown="span">Tachtsidis, I., & Scholkmann, F. (2016). False positives and false negatives in functional near-infrared spectroscopy: issues, challenges, and the way forward. *Neurophotonics*, 3(3), 031405. [link](https://doi.org/10.1117/1.NPh.3.3.031405)</td>
</tr>
<tr>
<td markdown="span">S01E04</td>
<td markdown="span">Cagiltay, B., & Mutlu, B. (2024). Toward Family-Robot Interactions: A Family-Centered Framework in HRI. In *Proceedings of the 2024 ACM/IEEE International Conference on Human-Robot Interaction* (pp. 76-85). [link](https://dl.acm.org/doi/10.1145/3610977.3634976)</td>
</tr>
<tr>
<td markdown="span">S01E05</td>
<td markdown="span">Quesque, F., Apperly, I., Baillargeon, R., Baron-Cohen, S., Becchio, C., Bekkering, H., ... & Brass, M. (2024). Defining key concepts for mental state attribution. *Communications Psychology*, 2(1), 29.[link](https://www.nature.com/articles/s44271-024-00077-6)</td>
</tr>
<tr>
<td markdown="span">S01E06</td>
<td markdown="span">Zawieska, K. (2024). The Iron Cage of Social Robotics. *ACM Transactions on Human-Robot Interaction.* [link](https://dl.acm.org/doi/10.1145/3695772)</td>
</tr>
<tr>
<td markdown="span">S01E07</td>
<td markdown="span">Rubegni, E., Malinverni, L., & Yip, J. (2022). “Don't let the robots walk our dogs, but it's ok for them to do our homework”: children's perceptions, fears, and hopes in social robots. In *Proceedings of the 21st Annual ACM Interaction Design and Children Conference *(pp. 352-361). [link](https://dl.acm.org/doi/10.1145/3501712.3529726)</td>
</tr>
<tr>
<td markdown="span">S01E08</td>
<td markdown="span">Traeger, M. L., Strohkorb, S., Jung, M., Scassellati, B., & Christakis, N. A. (2020). Vulnerable robots positively shape human conversational dynamics in a human–robot team. *Proceedings of the National Academy of Sciences*, 117(12), 6370–6375. [link](https://doi.org/10.1073/pnas.1910402117)</td>
</tr>
<tr>
<td markdown="span">S01E09</td>
<td markdown="span">Rabb, N., Law, T., Chita-Tegmark, M., & Scheutz, M. (2022). An attachment framework for human-robot interaction. International journal of social robotics, 1-21. [link](https://doi.org/10.1007/s12369-021-00802-9)</td>
</tr>
<tr>
<td markdown="span">S01E10</td>
<td markdown="span">Journal club extravaganza: the article edition. Bring the article you *think* everyone should read (complete list will be posted soon)</td>
</tr>
</tbody>
</table>

<br>   
**Season 2**:

<table>
<colgroup>
<col width="10%" />
<col width="90%" />  
</colgroup>
<thead>
<tr class="header">
<th>Episode</th>
<th>Article</th>
</tr>
</thead>
<tbody>
<tr>
<td markdown="span">S02E01</td>
<td markdown="span">Guingrich, R. E., & Graziano, M. S. (2024). Ascribing consciousness to artificial intelligence: human-AI interaction and its carry-over effects on human-human interaction. *Frontiers in Psychology*, 15, 1322781. [link](https://www.frontiersin.org/journals/psychology/articles/10.3389/fpsyg.2024.1322781/full)</td>
</tr>
<tr>
<td markdown="span">S02E02</td>
<td markdown="span">Levenson, R. W., & Ruef, A. M. (1992). Empathy: a physiological substrate. *Journal of Personality and Social Psychology*, 63(2), 234.[link](https://psycnet.apa.org/record/1992-42183-001)</td>
</tr>
<tr>
<td markdown="span">S02E03</td>
<td markdown="span">Pinti, P., Dina, L. M., & Smith, T. J. (2024). Ecological functional near-infrared spectroscopy in mobile children: using short separation channels to correct for systemic contamination during naturalistic neuroimaging. *Neurophotonics*, 11(4), 045004-045004. [link](https://www.spiedigitallibrary.org/journals/neurophotonics/volume-11/issue-4/045004/Ecological-functional-near-infrared-spectroscopy-in-mobile-children--using/10.1117/1.NPh.11.4.045004.full)</td>
</tr>
<tr>
<td markdown="span">S02E04</td>
<td markdown="span">Holroyd, C. B. (2022). Interbrain synchrony: on wavy ground. *Trends in Neurosciences*, 45(5), 346-357. [link](https://www.sciencedirect.com/science/article/pii/S0166223622000364)</td>
</tr>
<tr>
<td markdown="span">S02E05</td>
<td markdown="span">Vlasceanu, M., & Amodio, D. M. (2022). Propagation of societal gender inequality by internet search algorithms. *Proceedings of the National Academy of Sciences*, 119(29), e2204529119.[link](https://www.pnas.org/doi/full/10.1073/pnas.2204529119)</td>
</tr>
<tr>
<td markdown="span">S02E06</td>
<td markdown="span">Naselaris, T., Allen, E., & Kay, K. (2021). Extensive sampling for complete models of individual brains. *Current Opinion in Behavioral Sciences*, 40, 45-51.[link](https://www.sciencedirect.com/science/article/pii/S2352154620301960)</td>
</tr>
<tr>
<td markdown="span">S02E07</td>
<td markdown="span">de Felice, S. et al. (2025). Relational neuroscience: Insights from hyperscanning research. *Neuroscience & Biobehavioral Reviews*, 169, 105979. [link](https://doi.org/10.1016/j.neubiorev.2024.1059790)</td> 
</tr>
<tr>
<td markdown="span">S02E08</td>
<td markdown="span">TBA</td>
</tr>
<tr>
<td markdown="span">S02E09</td>
<td markdown="span">TBA</td>
</tr>
<tr>
<td markdown="span">S02E10</td>
<td markdown="span">Journal club extravaganza: the movie edition. Bring the movie you *think* everyone should watch </td>
</tr>
</tbody>
</table>