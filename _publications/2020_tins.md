---
citation: "Anna Henschel\\*, **Ruud Hortensius***, Emily S. Cross (2020). Social cognition in the age of human–robot interaction. <i>Trends in Neurosciences</i> <br> *<i>joined first authorship<i>"
date: '2020-12-24'
pdf: 'https://www.cell.com/action/showPdf?pii=S0166-2236%2820%2930073-4'
---
