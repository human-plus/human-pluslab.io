---
name: Fabiola Diana
startdate: [2012-01-01]
enddate: []
image: /static/img/members/fabi.jpeg
# altimage: /static/img/members/[file].jpg
position: PhD-candidate
#current:
#email: d.fabiola (at) fsw.leidenuniv.nl
#scholar: kOa9KygAAAAJ
#website: 
twitter: FabiolaDianaa
#github: 
#gitlab: 
#orcid: 0000-0002-5279-6202
description: "Fabiola is currently a Ph.D. candidate at Comparative Psychology and Affective Neuroscience Lab (CoPAN), 
at Leiden University. She works under the supervision of Mariska Kret and Ruud. Fabiola is broadly 
fascinated by how the socio-affective behavior has evolved and will continue to evolve throughout history, 
from bacteria's communication to contemporary artificial species. In her Ph.D. project, she is focusing on
 human-human physiological and behavioral synchrony and by what means these phenomena shape trust and cooperation 
 in interactions. Together with Ruud, she is also working on honesty and trust in human-robot interaction and on 
 investigating the robot's physical features that elicit more prosocial behavior. In her spare time, Fabiola likes
  to practice yoga, listen to progressive rock and give love to her plants. You can find her soaked in a book or one
   of her thousand interests.
   
"
---
