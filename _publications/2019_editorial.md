---
citation: "Emily S Cross\\*, <b>Ruud Hortensius*</b>, Agnieszka Wykowska\\* (2019). From social brains to social robots: applying neurocognitive insights to human–robot interaction. <i>Philosophical Transactions of the Royal Society B</i><br> *<i>joined first authorship<i>"

date: '2019-03-24'
pdf: 'https://royalsocietypublishing.org/doi/pdf/10.1098/rstb.2018.0024'
links:
- name: special issue
  url: https://royalsocietypublishing.org/toc/rstb/374/1771

---
