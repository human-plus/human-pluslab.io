---
name: Giulia d'Angelo 
startdate: [2012-01-01]
enddate: []
image: /static/img/members/giulia.jpg
# altimage: /static/img/members/[file].jpg
position: Student-assistant
#current:
#twitter: 
#github: 
#gitlab: 
#orcid: 
description: "Giulia obtained her Bachelor's degree at the University College of Utrecht 
with an interdisciplinary major in Psychology and Neuroscience. 
She is now continuing her studies with the Social and Health Psychology 
research master program. 
She started working with Ruud and other members of the Utrecht Young Academy 
on the creation of a platform providing 
clear and scientific answers to public's questions about AI; the AI helpdesk. 
The platform is being finalized as you're reading this and will be available online soon. In her spare time, Giulia plays volleyball, takes (many!) dance classes and loves spending time outdoors and with her friends!"
---
