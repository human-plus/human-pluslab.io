---
name: Ronja Held
startdate: [2012-01-01]
enddate: []
image: /static/img/members/ronja.jpeg
# altimage: /static/img/members/[file].jpg
position: PhD-candidate
#current:
#email: 
#scholar: 
#website: 
#twitter: 
#github: 
#gitlab: 
#orcid: 
description: "Ronja obtained a Bachelor's degree in Psychology followed by a 
Research Master's degree in Social and Economic Psychology from Leiden University. 
Currently, she works as a PhD candidate at Utrecht University under the supervision 
of Ruud. Throughout her academic journey, she developed an interest in the 
complex dynamics within and between groups. She conducted research on factors 
that promote ingroup cooperation while simultaneously potentially leading to 
conflict with an outgroup. During this research, she became intrigued by the 
subconscious mechanisms underlying social phenomena such as trust. 
As a result, she specialized further in physiological synchrony that 
arises during dyadic interactions. Additionally, she developed an affinity 
for using economic games to study social behavior.
In her current Ph.D. research, she investigates the long-term effect of the 
presence of an artificial agent at home on the social dynamics of families 
using fNIRS. In my free time, she enjoys hiking, playing tennis, and reading.
"
---
