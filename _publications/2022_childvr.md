---
citation: "Sofía Seinfeld, **Ruud Hortensius**, Jorge Arroyo-Palacios, Guillermo Iruretagoyena, Luis Zapata, Beatrice de Gelder, Mel Slater, Marie V Sanchez-Vives (2022). Immersing Offenders in a Virtual Domestic Violence Scene from a Child Perspective. <i>Journal of Interpersonal Violence</i> "
date: '2022-06-21'
pdf: 'https://journals.sagepub.com/doi/full/10.1177/08862605221106130'
---

  