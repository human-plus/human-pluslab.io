---
title: Human+ at InScience
author:   
layout: post
group: news
---
Luca and Ruud are super excited to be part of [InScience](https://www.insciencefestival.nl/en/), the international science festival at Nijmegen. 
During InScience we will showcasing two of our latest human-robot interaction experiments. Come and join us and two robots and let's explore the future of humans and machines together!

Quick teaser: 

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">Clicbot is ready for you at <a href="https://twitter.com/insciencefilm?ref_src=twsrc%5Etfw">@insciencefilm</a>! Come by for a fun day of research and robots with <a href="https://twitter.com/RuudHortensius?ref_src=twsrc%5Etfw">@RuudHortensius</a> and myself 🤖 <a href="https://t.co/wFb9VzxR9V">pic.twitter.com/wFb9VzxR9V</a></p>&mdash; Luca M. Leisten (@LucaLeisten) <a href="https://twitter.com/LucaLeisten/status/1458793153910214666?ref_src=twsrc%5Etfw">November 11, 2021</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>