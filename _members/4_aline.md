---
name: Aline Moore Lorusso
startdate: [2012-01-01]
enddate: []
image: /static/img/members/aline-2.jpg
# altimage: /static/img/members/[file].jpg
position: PhD-candidate
#current:
#twitter: 
#github: 
#gitlab: 
#orcid: 
description: "Aline is originally from Rome. But apparently for her not “all roads lead to Rome” 
since after high school, she decided to move away to complete a BSc in 
Psychology at Tilburg University and the Social and Health Psychology Research MSc at Utrecht University. 
Currently she is working as a PhD candidate under the supervision of 
Ruud and Esther Kluwer. 
Her interest in Psychology is manifold but she is mainly interested
 in social groups, their dynamics and what influences them which she 
 is currently researching in the context of families and AI.  
In her free time, you will probably find her on endless walks with her
dog, complaining about the Dutch weather, cooking way too much food and 
not knowing when to eat her leftovers. 
"
---
