---
title: Human+ journal club
layout: default
group: journal club
---

# Social Cognitive Aspects of Artificial Intelligence 
A joint-group meeting with the teams of Baptist and Ruud

### Schedule

<iframe src="https://docs.google.com/document/d/e/2PACX-1vQUKZFCgfEIypplJpgTlPBQjDzXyjP5_AlwCFEQbXlj58XqYNCgbudYPHq7Sf6jQTlHWpvxVTY5w4ny/pub?embedded=true" style="width:100%;height:800px"> </iframe>