---
name: Hester van Beek 
startdate: [2012-01-01]
enddate: []
image: /static/img/members/hester.jpg
# altimage: /static/img/members/[file].jpg
position: MSc student and student-assistant
#current:
#twitter: 
#github: 
#gitlab: 
#orcid: 
description: "Hester finished her BSc Psychology at Utrecht University and quickly realized her interest in the human brain. After doing a minor in social neuroscience and writing her thesis on theory-of-mind and social robots with the use of fNIRS, she is now pursuing her studies in the Neuroscience and Cognition MSc programme. She decided to follow her internship under Ruud’s supervision in which she is investigating cognitive and affective empathy for social robots. During her spare time, you can find her at the rowing club, painting, or at a restaurant with her friends."
---
