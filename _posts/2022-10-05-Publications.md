---
title: Publication festival
author:   
layout: post
group: news
---
In the last few weeks three papers were published! Two in International Journal of Social Robotics and one in European Journal of Neuroscience. 
Super excited to see these open-access publications, with all code available, appear in these great journals. Also, amazing work of all students involved. 

Also, Ruud joined International Journal of Social Robotics as an associate editor (of course, the two articles appearing in this journal underwent peer review before this)


Paper 1 on attitudes with Fabi: <a href="https://link.springer.com/article/10.1007/s12369-022-00917-7/">IJSR</a>  
Paper 2 on chemosignaling and HRI: <a href="https://link.springer.com/article/10.1007/s12369-022-00918-6">IJSR</a>   
Paper 3 on neural network involvement during HRI with Ann: <a href="https://onlinelibrary.wiley.com/doi/10.1111/ejn.15823">EJN</a>