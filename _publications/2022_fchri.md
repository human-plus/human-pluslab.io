---
citation: "**Ruud Hortensius**, **Ann Hogenhuis** (2022). Domain-specific and domain-general neural network engagement during human-robot interactions.  <i>European Journal of Neuroscience</i> "
date: '2022-09-16'
pdf: 'https://onlinelibrary.wiley.com/doi/10.1111/ejn.15823'
links: 
- name: preprint
  url: https://psyarxiv.com/rus2w/
- name: data
  url: https://osf.io/dby4j/
- name: code
  url: https://gitlab.com/human-plus/fchri/ 
---