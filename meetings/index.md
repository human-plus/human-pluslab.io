---
title: Lab meeting
layout: default
group: meetings
---

# Lab meetings


We have a weekly lab meeting, in which lab members present their ongoing projects, give tutorials (e.g., WTC analysis, working with Git) 
or in which we have group brain storms and discussions (e.g, on the use of generative AI in research). 
We use a flexible format. Below you will find a an overview of topics we have discussed previously. We also regularly invite guest speakers. We especially want to give early career 
researchers room to present, so if you work on something close to our research, do let us know! Also, if you want to join, let us know! 

**Semester II 2026-2025**

<table>
<colgroup>
<col width="20%" />
<col width="30%" />
<col width="50%" />
</colgroup>
<thead>
<tr class="header">
<th>Meeting</th>
<th>Presenter</th>
<th>Content</th>
</tr>
</thead>
<tbody>
<tr>
<td markdown="span">January meeting 1</td>
<td markdown="span">Everyone</td>
<td markdown="span">Goal setting first half 2025</td>
</tr>
<tr>
<td markdown="span">January meeting 2</td>
<td markdown="span">Fabiola</td>
<td markdown="span">Hyperscanning study idea</td>
</tr>
<tr>
<td markdown="span">January meeting 3</td>
<td markdown="span">Dorka + Everyone</td>
<td markdown="span">Cool presentation + Generative AI round table (introduction)</td>
</tr>
<tr>
<td markdown="span">January meeting 4</td>
<td markdown="span">Luna + Everyone</td>
<td markdown="span">Hyperscanning study idea + Generative AI round table (continued)</td>
</tr>
<tr>
<td markdown="span">February meeting 1</td>
<td markdown="span">Reinoud Kaldewaij (guest)</td>
<td markdown="span">The neurophysiological mechanisms of touch in cooperation</td>
</tr>
<tr>
<td markdown="span">February meeting 2</td>
<td markdown="span">Zhen + Everyone</td>
<td markdown="span">Thesis project idea + Discussion on errors and lab procedures. Based on Error-tight: Exercises for lab groups to prevent research mistakes. </td>
</tr>
<tr>
<td markdown="span">February meeting 3</td>
<td markdown="span">Rebecca Wald (guest)</td>
<td markdown="span">Virtual assistants in the family environment</td>
</tr>
<tr>
<td markdown="span">February meeting 4</td>
<td markdown="span">Navya Sharan (guest)</td>
<td markdown="span">Computers as social actors</td>
</tr>
<tr>
<td markdown="span">March meeting 1</td>
<td markdown="span">Hester + Fabiola</td>
<td markdown="span">Task ideas super-hyperscanning study + project update</td>
</tr>
<tr>
<td markdown="span">March meeting 2</td>
<td markdown="span">Luna + Zhen</td>
<td markdown="span">Final research proposals (both)</td>
</tr>
<tr>
<td markdown="span">March meeting 3</td>
<td markdown="span">Dilys Eikelboom (guest)</td>
<td markdown="span">Mobile lab demo</td>
</tr>
<tr>
<td markdown="span">March meeting 4</td>
<td markdown="span">Dorka</td>
<td markdown="span">EEG-fNIRS study</td>
</tr>
<tr>
<td markdown="span">March meeting 5</td>
<td markdown="span">Ruud</td>
<td markdown="span">Musings on small-n studies and precision neuroscience</td>
</tr>
<tr>
<td markdown="span">March meeting 6</td>
<td markdown="span">Anna Pörnbacher (guest)</td>
<td markdown="span">Trait empathy and concern for animal welfare (meta-analysis)</td>
</tr>
</tbody>
</table>

<br>   

**Semester I 2026-2025**

<table>
<colgroup>
<col width="20%" />
<col width="30%" />
<col width="50%" />
</colgroup>
<thead>
<tr class="header">
<th>Meeting</th>
<th>Presenter</th>
<th>Content</th>
</tr>
</thead>
<tbody>
<tr>
<td markdown="span">September meeting 1</td>
<td markdown="span">Fabi</td>
<td markdown="span">Cozmo project</td>
</tr>
<tr>
<td markdown="span">October meeting 1</td>
<td markdown="span">Dorka + Ann</td>
<td markdown="span">Betweter results + Project application</td>
</tr>
<tr>
<td markdown="span">October meeting 2</td>
<td markdown="span">Ruud</td>
<td markdown="span">Working with Git</td>
</tr>
<tr>
<td markdown="span">October meeting 3</td>
<td markdown="span">Fabi</td>
<td markdown="span">PhD overview</td>
</tr>
<tr>
<td markdown="span">October meeting 4</td>
<td markdown="span">Ronja</td>
<td markdown="span">PhD update</td>
</tr>
<tr>
<td markdown="span">October meeting 5</td>
<td markdown="span">Ann</td>
<td markdown="span">Introduction presentation</td>
</tr>
<tr>
<td markdown="span">November meeting 1</td>
<td markdown="span">Aline, Ronja, and Ann</td>
<td markdown="span">Testing tasks</td>
</tr>
<tr>
<td markdown="span">November meeting 2</td>
<td markdown="span">Hester</td>
<td markdown="span">Final presentation research project Hester</td>
</tr>
<tr>
<td markdown="span">November meeting 3</td>
<td markdown="span">Dorka and Hester</td>
<td markdown="span">fnirs4all: introduction and focus group</td>
</tr>
<tr>
<td markdown="span">November meeting 4</td>
<td markdown="span">Aline</td>
<td markdown="span">Conspiracy paper</td>
</tr>
<tr>
<td markdown="span">December meeting 1</td>
<td markdown="span">Everyone</td>
<td markdown="span">Planning fNIRS first half 2025</td>
</tr>
<tr>
<td markdown="span">December meeting 2</td>
<td markdown="span">Sonia Paternò (guest)</td>
<td markdown="span">tACS-fNIRS project presentation</td>
</tr>
<tr>
<td markdown="span">December meeting 3</td>
<td markdown="span">Everyone</td>
<td markdown="span">End of year review</td>
</tr>
</tbody>
</table>

<br>   




<br> 


The schedule for the 2023-2024  Semester II will be posted shortly.