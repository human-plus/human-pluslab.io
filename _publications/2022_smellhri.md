---
citation: " Ilja Croijmans, Laura van Erp, Annelie Bakker, Lara Cramer, Sophie Heezen, Dana Van Mourik, Sterre Weaver, **Ruud Hortensius** (2022). No effect of the smell of hexanal on trust in human-robot interaction. <i>International Journal of Social Robotics</i> "
date: '2022-09-15'
pdf: 'https://link.springer.com/article/10.1007/s12369-022-00918-6'
links:
- name: preprint
  url: https://psyarxiv.com/zqsr4
- name: data, materials & code
  url: ttps://osf.io/ys8na/
- name: preregistration
  url: https://osf.io/dcvzn
---