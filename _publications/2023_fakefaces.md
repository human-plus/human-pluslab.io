---
citation: "Baptist Liefooghe\\*, **Manuel Oliveira***, <b>Luca M. Leisten</b>, Eline Hoogers, Henk Aarts, <b>Ruud Hortensius</b> (2023). Faces Merely Labelled as Artificial are Trusted Less. <i>Collabra: Psychology</i> "
date: '2023-03-27'
pdf: https://online.ucpress.edu/collabra/article/9/1/73066/195773/Are-Natural-Faces-Merely-Labelled-as-Artificial
links:
- name: preprint
  url: https://psyarxiv.com/te2ju
- name: data, materials & code
  url: https://osf.io/3hrx2
- name: preregistrations
  url: https://osf.io/w4bca
---
