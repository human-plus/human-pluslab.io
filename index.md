---
title: Human+
layout: home
group: home
---

Welcome! We are **Human+**, a team started by [Dr. Ruud Hortensius](https://www.ruudhortensius.nl), trying to understand human social cognition in the age of artificial intelligence. We are part of the [Department of Psychology](https://www.uu.nl/en/organisation/faculty-of-social-and-behavioural-sciences/about-the-faculty/departments#psychology) at Utrecht University in The Netherlands.
{: .welcomefont}

<img src="static/img/members/group-2024-october.jpg" alt="drawing" class="right" height="400" >

We are happy to be part of a great inter- and multidisciplinary collaborative research environment at Utrecht University and collaborate with scholars from the [Psychology department](https://www.uu.nl/en/organisation/social-health-organisational-psychology), [Human-centered AI focus area](https://www.uu.nl/en/research/human-centered-artificial-intelligence), the [Human-AI alliance](https://human-ai.nl/), and the [Embodied AI initiative](https://embodiedaiuu.wixsite.com/home). 
{: .welcomefont}

Our research is generously funded by:
{: .welcomefont}


<img src="static/img/logo/erc.png" alt="drawing" width="100"/>
<img src="static/img/logo/nwo.jpeg" alt="drawing" width="200"/>
<img src="static/img/logo/uu.png" alt="drawing" width="200"/>

*[major updates coming]*
