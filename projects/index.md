---
title: Student projects
layout: default
group: projects
---

# Available Social Cognitive Aspects of Artificial Intelligence student-projects
Below you will find a list of projects supervised by Baptist Liefooghe and/or Ruud Hortensius that are available for BSc and MSc-students. Do contact the person listed for each project if you are interested and want to discuss possibilities. 

<iframe src="https://docs.google.com/document/d/e/2PACX-1vQglS-6OLpwshtocCr0So_Xz4XArcE6_RT23uhPwlMQrauWhSjhj_x_2_BeANC3cyQkxq9KxAzs9Gq9/pub?embedded=true" style="width:80%;height:800px"> </iframe>