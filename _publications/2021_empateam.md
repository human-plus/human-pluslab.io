---
citation: "Dorina de Jong, **Ruud Hortensius**, Te-Yi Hsieh, Emily S. Cross (2021). Empathy and schadenfreude in human-robot teams. <i>Journal of Cognition</i>"
date: '2021-02-24'
pdf: 'https://www.journalofcognition.org/articles/10.5334/joc.177/'
links:
- name: preprint
  url: https://psyarxiv.com/2wyxp
- name: preregistration
  url: https://osf.io/nceqp
- name: data & code
  url: https://osf.io/ax4dh/  
---
