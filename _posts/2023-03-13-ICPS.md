---
title: The future of psychology and robotics at ICPS23
author:   
layout: post
group: news
---

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">A great privilege to be part of this symposium on the future of psychology and robots. Thanks to <a href="https://twitter.com/socialneuro?ref_src=twsrc%5Etfw">@socialneuro</a> chairing this session! <a href="https://twitter.com/hashtag/icps23be?src=hash&amp;ref_src=twsrc%5Etfw">#icps23be</a> was inspiring, so great to be part of this interdisciplinary community! <a href="https://t.co/c7r3b9gRJb">https://t.co/c7r3b9gRJb</a></p>&mdash; Ruud Hortensius @ruudhortensius@fediscience.org (@RuudHortensius) <a href="https://twitter.com/RuudHortensius/status/1635222339746164736?ref_src=twsrc%5Etfw">March 13, 2023</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>