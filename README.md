# human-plus.gitlab.io
====================

Human+ website


Technologies this website uses:  

    Jekyll  
    Github Pages  
    Twitter Bootstrap 4.4.1

Install for mac:

[Follow these steps](https://jekyllrb.com/docs/installation/macos/)

Before pushing changes, please check that they will work on your system first with the plugins included in the Gemfile using the bundler tool (results served at [0.0.0.0:4000](0.0.0.0:4000)):

    gem install bundler
    bundle install
    bundle exec jekyll serve
    
You might need to do:

	bundle add webrick

This site is adapted form: 
https://github.com/fraser-lab/fraser-lab.github.io   
https://github.com/hilaryrichardson/hilaryrichardson.github.io
