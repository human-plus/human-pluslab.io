---
name: Zhen Cong Ng 
startdate: [2012-01-01]
enddate: []
image: /static/img/members/zhen.jpeg
# altimage: /static/img/members/[file].jpg
position: MSc student
#current:
#twitter: 
#github: 
#gitlab: 
#orcid: 
description: "Zhen obtained his BSc in Psychology from the University of Amsterdam (UvA), 
where an interdisciplinary minor sparked his interest in Human-AI interaction. 
Now following the Psychology Research Masters' at the UvA, with a major in Developmental Psychology, 
he is writing his thesis under Ruud's supervision inb the Human+ lab, where
he will investigate human-AI group dynamics during competitive and cooperative settings.
 Zhen was an assistant Scout leader and has participated in international camps in the U.S., Switzerland, and South Korea 
 (the South Korean camp evacuation was not his fault). Despite his love for Asian cuisine, 
 Zhen's favourite kitchen appliance is the air fryer.  
"
---
