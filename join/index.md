---
title: Join our group
layout: default
group: join
---

# Join our team!


We are always seeking interested students and researchers from a diverse background that want to join the fun. Human+ is interdisciplinarity, diversity, and an open vision. There are many different ways to join the fun! New opportunities will be posted on this page when available (think postdoc, research and student assistantships). Several other student-driven opportunities are listed below. Do contact Ruud if you are keen to explore these! Of course you can talk to current/former students. 

*Interships*   
Erasmus+ and DAAD are great ways for European students to join Human+. Ruud has worked with the DAAD programme for several years, and German students interested in a [DAAD RISE internship](https://www.daad.de/rise/en/rise-worldwide/) can apply via the DAAD internship portal (deadline in December). The [Erasmus+ programme](https://erasmus-plus.ec.europa.eu/) offers flexible grants for students to join for a short-term stay. No unpaid internships are offered for non-UU students because of unfairness to the student. For UU-students, internship options will be listed here (currently, no internships available). 

*BSc and MSc thesis*   
Students enrolled in the Psychology, Artificial Intelligence, Liberal Arts and Sciences, or University College Utrecht programmes can complete their thesis and/or projects through their respective programme. Members of the lab are often on the list of potential supervisors. Do check with your program if any opportunities exists. 

*PhD student*   
Currently, there is no lab-funded PhD position available. Ruud is happy to discuss other opportunities, for instance university or external funding or student-led opportunities (e.g. Talent schemes).

*Postdoc*    
Currently, there is no postdoc position available. Ruud is happy to support individual applications for external funding by talented candidates that align with the Human+ research interests. Do contact him about your opportunity. Examples of external funding include: [Marie Skłodowska-Curie Actions](https://ec.europa.eu/research/mariecurieactions/actions/postdoctoral-fellowships);
[the DAAD+](https://www.daad.de/en/study-and-research-in-germany/scholarships/); and [the Alexander von Humboldt Foundation+](https://www.humboldt-foundation.de/en/). +for candidates coming from Germany or with a German nationality.