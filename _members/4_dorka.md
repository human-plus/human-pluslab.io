---
name: Dorka Boda
startdate: [2012-01-01]
enddate: []
image: /static/img/members/dorka.jpg
# altimage: /static/img/members/[file].jpg
position: Research assistant
#current:
#twitter: 
#github: 
#gitlab: 
#orcid: 
description: "Dorka completed her Bachelor’s degree in Psychology in Budapest, Hungary. After obtaining her 
research Master’s degree in Social and Health Psychology at Utrecht University, she is now working as a research assistant. 
In her Master’s thesis, she investigated brain activity during real-life social interactions with animals and AI, 
where she developed a strong interest in the fNIRS neuroimaging technique. 
This project quickly became her main focus, particularly in terms of its 
methodology and technical aspects. This year, she is joining the fnirs4all project, 
which aims to create an accessible and open resource for researchers and students 
interested in incorporating fNIRS into their studies. Dorka is excited to combine 
her passion for fNIRS with her love for teaching and sharing knowledge. 
Outside of the lab, you’ll likely find her outdoors, either on a climbing wall 
or running somewhere around the city.
 
"
---
