---
title: Fabi in Japan		
author:   
layout: post
group: news
---
Fabi received a JSPS fellowship for a research project combining cross-cultural effects, pupil size, eye tracking, and human-robot interaction! Super exciting work together with Mariska Kret.

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">Starting my JSPS-founded fellowship just in time for <a href="https://twitter.com/hashtag/hanami?src=hash&amp;ref_src=twsrc%5Etfw">#hanami</a>. Excited for my cross-cultural study on the effect of pupil size on honesty in human-robot interaction.<br>So grateful to conduct my research in the stunning Tokyo 🌺<a href="https://twitter.com/jsps_sns?ref_src=twsrc%5Etfw">@jsps_sns</a> <a href="https://twitter.com/RuudHortensius?ref_src=twsrc%5Etfw">@RuudHortensius</a> <a href="https://twitter.com/MariskaKret?ref_src=twsrc%5Etfw">@MariskaKret</a> <a href="https://twitter.com/CoPAN_Leiden?ref_src=twsrc%5Etfw">@CoPAN_Leiden</a> <a href="https://t.co/tPoNSCO7U8">pic.twitter.com/tPoNSCO7U8</a></p>&mdash; Fabiola Diana (@FabiolaDianaa) <a href="https://twitter.com/FabiolaDianaa/status/1632787521590681600?ref_src=twsrc%5Etfw">March 6, 2023</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>