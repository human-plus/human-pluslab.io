---
citation: "**Ruud Hortensius\\***, Bishakha Chaudhury\\*, Martin Hoffmann, Emily S. Cross (2022). Tracking human interactions with a commercially-available robot over multiple Days: A tutorial [version 1; peer review: 1 approved, 2 approved with reservations]. <i>Open Research Europe: Artificial Intelligence and the Social Sciences and Humanities</i> "
date: '2022-08-16' 
pdf: 'https://open-research-europe.ec.europa.eu/articles/2-97/v1'
links:
- name: preprint
  url: https://psyarxiv.com/fd3h2/
- name: code
  url: https://github.com/cozmo4hri
- name: shiny app
  url: https://shiny.psy.gla.ac.uk/ruudh/interactive-tool/
---

         