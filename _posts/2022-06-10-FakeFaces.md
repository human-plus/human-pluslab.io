---
title: Mere label effect
author:   
layout: post
group: news
---
Very excited to share a new preprint as part on a new research line and collaboration with Baptist Liefooghe and Henk Aarts on trust and artificial intelligence. What is the psychological impact of content created by artificial intelligence? Or as Manuel says: "Would you trust a real face less if you were told it was artificial?"
Read the <a href="https://psyarxiv.com/te2ju/">preprint</a>, explore the <a href="https://osf.io/3hrx2/">data</a> or read the <a href="https://osf.io/w4bca/">preregistration</a>.

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">📢New preprint alert! 📢 w/ Baptist Liefooghe, <a href="https://twitter.com/LucaLeisten?ref_src=twsrc%5Etfw">@LucaLeisten</a>, Eline Hoogers, <a href="https://twitter.com/henk_aart?ref_src=twsrc%5Etfw">@henk_aart</a>, &amp; <a href="https://twitter.com/RuudHortensius?ref_src=twsrc%5Etfw">@RuudHortensius</a>. Would you trust a real face less if you were told it was artificial? Find out more👇 <a href="https://t.co/qrskfUDH9v">https://t.co/qrskfUDH9v</a></p>&mdash; Manuel Oliveira (@manueljbo) <a href="https://twitter.com/manueljbo/status/1535277373331054595?ref_src=twsrc%5Etfw">June 10, 2022</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>