---
title: Embodied AI talk
author:   
layout: post
group: news
---
Very excited to host professor Ana Paiva (University of Lisbon) during the monthly Embodied AI seminar series (25 November 16:00 CET). The tile of the talk is 
'Engineering sociality and Collaboration: Humans and Embodied Agents Together’. More information on the talk can be found on [our website](https://embodiedaiuu.wixsite.com/home), [twitter](https://twitter.com/EmbodiedAI_UU) 
and on the [UU website](https://uu.nl/en/events/first-talk-in-embodied-ai-talk-series).

For upcoming talks, do check our [twitter feed!](https://twitter.com/EmbodiedAI_UU) 

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">We are very excited to announce the upcoming <a href="https://twitter.com/EmbodiedAI_UU?ref_src=twsrc%5Etfw">@EmbodiedAI_UU</a> talks! 🤖 We will welcome <a href="https://twitter.com/SeinfeldSofia?ref_src=twsrc%5Etfw">@SeinfeldSofia</a>, <a href="https://twitter.com/eduardfosch?ref_src=twsrc%5Etfw">@eduardfosch</a>, <a href="https://twitter.com/SoCogInt?ref_src=twsrc%5Etfw">@SoCogInt</a>, and <a href="https://twitter.com/brain_on_dance?ref_src=twsrc%5Etfw">@brain_on_dance</a>! <a href="https://t.co/pt4BbFDUcZ">pic.twitter.com/pt4BbFDUcZ</a></p>&mdash; Embodied AI @ Utrecht University (@EmbodiedAI_UU) <a href="https://twitter.com/EmbodiedAI_UU/status/1472967449268174854?ref_src=twsrc%5Etfw">December 20, 2021</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>