---
title: Embodied AI initiative
author:   
layout: post
group: news
---
Together with Maartje de Graaf and Zerrin Yumak, Ruud started the Embodied AI initiative in 2020. 
The main objective of this project is to unite researchers of Utrecht University and elsewhere in the field of Embodied AI. 
We aim to increase visibility, build community and foster interdisciplinary collaborations as well as encourage 
diversity and inclusion of perspectives and backgrounds. Thanks to funding from the Human-Centered AI focus area, Geertje will join us as a student assistant.
We have organised three panels and have now started a monthly seminar series. For more information 
visit [our website](https://embodiedaiuu.wixsite.com/home) or [follow us on twitter](https://twitter.com/EmbodiedAI_UU).



