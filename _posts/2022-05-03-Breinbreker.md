---
title: Operatie Breinbreker
author:   
layout: post
group: news
---
We participated in Operatie Breinbreker, a science show in TivoliVredenburg organised by Utrecht University! A great venue as scientists are the new rockstars. We took part with a fun game for children and cozmo robots alike 

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">Saturday we were part of <a href="https://twitter.com/hashtag/operatiebreinbreker?src=hash&amp;ref_src=twsrc%5Etfw">#operatiebreinbreker</a> organised by <a href="https://twitter.com/UniUtrechtPE?ref_src=twsrc%5Etfw">@UniUtrechtPE</a> in <a href="https://twitter.com/TiVre_Utrecht?ref_src=twsrc%5Etfw">@TiVre_Utrecht</a>. What a blast, so much fun, only smiling kids, parents, 🤖 and <a href="https://twitter.com/hashtag/humanplus?src=hash&amp;ref_src=twsrc%5Etfw">#humanplus</a> researchers. <a href="https://t.co/oLtNC0n63I">pic.twitter.com/oLtNC0n63I</a></p>&mdash; Ruud Hortensius (@RuudHortensius) <a href="https://twitter.com/RuudHortensius/status/1521403704712343552?ref_src=twsrc%5Etfw">May 3, 2022</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>