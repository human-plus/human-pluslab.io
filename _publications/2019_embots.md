---
citation: "Emily S Cross, Katie A Riddoch, Jaydan Pratts, Simon Titone, Bishakha Chaudhury, <b>Ruud Hortensius</b> (2019). A neurocognitive investigation of the impact of socialising with a robot on empathy for pain. <i>Philosophical Transactions of the Royal Society B<i>"
date: '2019-03-11'
pdf: 'https://royalsocietypublishing.org/doi/pdf/10.1098/rstb.2018.0034'
links:
- name: preprint
  url: https://www.biorxiv.org/content/early/2018/11/29/470534
- name: preregistration
  url: https://aspredicted.org/ng76j.pdf
- name: data
  url: https://osf.io/9h4n7/
- name: NeuroVault
  url: https://neurovault.org/collections/4096/
 
---
