---
name: Fabíola de Andrade 
startdate: [2012-01-01]
enddate: []
image: /static/img/members/fabíola.jpeg
# altimage: /static/img/members/[file].jpg
position: MSc student
#current:
#twitter: 
#github: 
#gitlab: 
#orcid: 
description: "Fabíola is originally from Brazil, but she feels more like a citizen of the world. 
After completing a conversion MSc in Psychology at the University of Glasgow, she is currently a 
student at the Neuroscience and Cognition research master at Utrecht University. Previously, she 
has also followed a Social Neuroscience minor in Utrecht, where she worked with functional near-infrared spectroscopy (fNIRS) 
investigating human-robot interactions. In her internship at the human+ lab, she aims to research human interactions 
with AI-powered chatbots. With an everlasting curiosity, Fabíola loves to learn and when she is not actively engaged 
in any learning endeavors, she likes to attend film festivals, meditate and being close to nature.
"
---
