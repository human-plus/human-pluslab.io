---
title: tomdognirs
author:   
layout: post
group: news
---
Sneak peek of ongoing science in the lab. Dorka just finished collecting data for a project on mentalizing across agents and species. For this we brought a real dog into the lab! So much fun! 

<img src="fNIRS-dog-scienceisgreat.png" alt="drawing" width="600"/>