---
title: Rising Star
author:   
layout: post
group: news
---
Humblebrag, Ruud has been named a Rising Star by the APS!!! 

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">Very happy to have been named a Rising Star by the APS.😁😁😁 Thanks to <a href="https://twitter.com/brain_on_dance?ref_src=twsrc%5Etfw">@brain_on_dance</a> and <a href="https://twitter.com/henk_aart?ref_src=twsrc%5Etfw">@henk_aart</a> for the nomination and support. Big shout out to all my colleagues, collaborators and students. Also I&#39;m terrible at updating info: I&#39;m at <a href="https://twitter.com/UniUtrecht?ref_src=twsrc%5Etfw">@UniUtrecht</a>! <a href="https://t.co/EE12m1Lfr4">https://t.co/EE12m1Lfr4</a></p>&mdash; Ruud Hortensius @ruudhortensius@fediscience.org (@RuudHortensius) <a href="https://twitter.com/RuudHortensius/status/1620733345792684033?ref_src=twsrc%5Etfw">February 1, 2023</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>