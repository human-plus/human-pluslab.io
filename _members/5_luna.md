---
name: Luna Walls 
startdate: [2012-01-01]
enddate: []
image: /static/img/members/luna.jpg
# altimage: /static/img/members/[file].jpg
position: MSc student
#current:
#twitter: 
#github: 
#gitlab: 
#orcid: 
description: "Luna followed a BSc Psychology at Utrecht University where she mainly focused on social and neuropsychology. 
She developed a passion for scientific research and an interest in social neuroscientific questions. 
After her BSc, she enrolled in the Neuroscience and Cognition master program in Utrecht. 
As part of this program, she now is doing an internship under Ruud's supervision at the Human+ lab. 
During her internship, she will investigate interbrain synchrony during group interaction using fNIRS. 
When she is not thinking of crazy impossible research ideas, 
she is at home playing Dungeons and Dragons with her friends, sewing, or on the couch watching a movie with a good craft beer."
---
