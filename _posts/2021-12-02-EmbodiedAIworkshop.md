---
title: Embodied AI workshop
author:   
layout: post
group: news
---
The Embodied AI initiative keeps growing. Thanks to funding from the Human-Centered AI focus area, we will organise a workshop on Embodied AI in 2023. Keep up to date on the latest Embodied AI and 
visit [our website](https://embodiedaiuu.wixsite.com/home) or [follow us on twitter](https://twitter.com/EmbodiedAI_UU).



