---
citation: "**Fabiola Diana**, Lola Cañamero, **Ruud Hortensius**, Mariska kret (2024). Merging sociality and robotics through an evolutionary perspective. <i>Science Robotics</i> "
date: '2024-07-24' 
pdf: https://www.science.org/doi/abs/10.1126/scirobotics.adk6664
links:
- name: postprint
  url: https://drive.google.com/file/d/1QMWHGXOqzSEoJ6NnQXeEzafOAP_NFeD8/view

---
