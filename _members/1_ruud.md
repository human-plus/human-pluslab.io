---
name: dr. Ruud Hortensius
startdate: [2010-01-01]
enddate: []
image: /static/img/members/Ruud-1.jpg
# altimage: /static/img/members/[file].jpg
position: Principal Investigator
#current:
#pronouns: he/him/his
#email: r.hortensius (at) uu.nl
#scholar: kOa9KygAAAAJ
#website: http://ruudhortensius.nl
#twitter: ruudhortensius
#github: rhortensius
#gitlab: hortensius
#orcid: 0000-0002-5279-6202
description: "Before starting his academic career, Ruud spent his childhood in and around 
the woods and floodplains of Elst. He was an underachiever in secondary school and was 
retained one year, but eventually managed to receive his high school diploma. After a 
BSc in in Social Work, he completed a BSc in Psychology (cum laude) and an MSc in Cognitive 
Neuroscience at Utrecht University (NL). In 2016, he obtained his PhD cum laude from Tilburg University (NL). 
Before joining Utrecht University as an assistant professor and later as associate professor, he travelled around the (academic) world as a postdoc 
at the University of Cape Town (South Africa), Bangor University (Wales), and the University of Glasgow (Scotland).
 His research and teaching focusses on social cognition in the age of the algorithm. Using a social neuroscience 
 perspective he tries to answer the question how real interactions with people, robots, and AI, affect
  everyday social cognition? In his spare time, he is an avid reader, overenthusiastic hockey player,  
  an still emerging boulderer, and a Smiths enthusiast. "
---
