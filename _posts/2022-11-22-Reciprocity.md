---
title: Reciprocity and CRI
author:   
layout: post
group: news
---
Super excited to share a new preprint called 'Children’s Reciprocity and Relationship Formation with a Robot Across Age' !
This work lead by Luca as part of her MSc-thesis and a first step into Child-Robot Interaction. Luca created a physical board game to measure reciprocity during actual child-robot interaction!!! 
As always you can read the <a href="https://osf.io/8au6v/">preprint</a>, explore the <a href="https://osf.io/z9kxb/">data</a> or read the <a href="https://osf.io/5djbc/">preregistration</a>.

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">Our new preprint on children&#39;s reciprocity and relationship formation with a robot is out!🤖 We looked at children aged 5-12 and found age to be an important factor in children&#39;s behavior towards a social robot. Go check it out!🌟 <a href="https://twitter.com/RuudHortensius?ref_src=twsrc%5Etfw">@RuudHortensius</a><a href="https://twitter.com/ev_heys?ref_src=twsrc%5Etfw">@ev_heys</a><a href="https://t.co/1GVCeKD3ds">https://t.co/1GVCeKD3ds</a></p>&mdash; Luca M. Leisten (@LucaLeisten) <a href="https://twitter.com/LucaLeisten/status/1592075771736371200?ref_src=twsrc%5Etfw">November 14, 2022</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
